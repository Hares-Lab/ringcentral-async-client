import os
from distutils import dir_util
from distutils import log
from distutils.cmd import Command
from distutils.command.build import build as BuildCommand
from distutils.command.clean import clean as CleanCommand
from typing import *

from pkg_resources import require

def safe_remove_tree(dir: str, *args, **kwargs):
    try: dir_util.remove_tree(dir, *args, **kwargs)
    except Exception: pass

def generate_commands(NAME, MODULE_NAME) -> Dict[str, Type[Command]]:
    
    class ExtendedCleanCommand(CleanCommand):
        description = "Clean all generated data, including built packages or code-gen"
        user_options: List[str] = \
            CleanCommand.user_options + \
            [
                ('eggs-base=', 'e', "Base directory for the eggs to be stored"),
                ('dist-base=', 'd', "Base directory for the built distributives to be stored"),
                ('codegen-base=', 'c', "Base directory for the code-gen to be stored"),
            ]
        
        cwd: Optional[str]
        eggs_base: Optional[str]
        dist_base: Optional[str]
        codegen_base: Optional[str]
        
        def initialize_options(self):
            self.cwd = None
            self.eggs_base = None
            self.codegen_base = None
            self.dist_base = 'dist'
            super().initialize_options()
        def finalize_options(self):
            self.cwd = os.getcwd()
            super().finalize_options()
            self.set_undefined_options('codegen', ('codegen_base', 'codegen_base'), ('eggs_base', 'eggs_base'))
        def run(self):
            assert os.getcwd() == self.cwd, 'Must be in package root: %s' % self.cwd
            super().run()
            safe_remove_tree(self.codegen_base, verbose=self.verbose, dry_run=self.dry_run)
            
            if (self.all):
                safe_remove_tree(self.dist_base, verbose=self.verbose, dry_run=self.dry_run)
                safe_remove_tree(self.eggs_base, verbose=self.verbose, dry_run=self.dry_run)
    
    class CodeGenCommand(Command):
        description = "custom clean command that forcefully removes dist/build directories"
        user_options: List[str] = \
        [
            ('eggs-base=', 'e', "Base directory for the eggs to be stored"),
            ('codegen-base=', 'c', "Base directory for the code-gen to be stored"),
            ('force', 'f', "Force code generation if it is already presented"),
        ]
        cwd: Optional[str]
        eggs_base: Optional[str]
        codegen_base: Optional[str]
        boolean_options = [ 'force' ]
        force: Optional[bool]
        
        def initialize_options(self):
            self.cwd = None
            self.force = None
            self.eggs_base = '.eggs'
            self.codegen_base = f'src/{MODULE_NAME}/_code_gen'
        def finalize_options(self):
            self.cwd = os.getcwd()
        def run(self):
            assert os.getcwd() == self.cwd, 'Must be in package root: %s' % self.cwd
            if (os.path.isfile(os.path.join(self.codegen_base, '__init__.py'))):
                if (not self.force):
                    log.debug("'%s' is already presented", self.codegen_base)
                    return
                else:
                    log.debug("'%s' is already presented, but will be re-created", self.codegen_base)
            
            require('openapi-parser>=0.2.4')
            from openapi_parser.cli import run
            log.info("Generating '%s'...", self.codegen_base)
            run('swagger/rc-platform.openapi-3.0.json', self.codegen_base, 'ringcentral-client', dry_run=self.dry_run, verbose=False)
    
    class ExtendedBuildCommand(BuildCommand):
        def run(self):
            self.run_command('codegen')
            super().run()
    
    cmd_class = dict \
    (
        clean = ExtendedCleanCommand,
        codegen = CodeGenCommand,
        build = ExtendedBuildCommand,
    )
    return cmd_class


__all__ = \
[
    'generate_commands',
]
